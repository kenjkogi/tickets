/**
 * Created by KEnneth Kogi on 5/2/15.
 */


var gulp = require('gulp')
var concat = require('gulp-concat')
var less = require('gulp-less')
//var image = require('gulp-image');


gulp.task('local', ['js:build-local', 'css:build-local', 'templates:build-local', 'images:build-local', 'fonts:build-local', 'fdata:build-local'])



gulp.task('build', ['js:build', 'css:build', 'templates:build'])

//gulp.task('css:build', function() {
//    gulp.src([
//        'fe-assets/css/**/bootstrap.min.css',
//        'fe-assets/css/**/*.css'])
//        .pipe(concat('style.css'))
//        .pipe(gulp.dest('./public/stylesheets'))
//})
//gulp.task('js:build', function() {
//    gulp.src([
//        'fe-assets/lib/**/jquery-1.11.2.min.js',
//        'fe-assets/lib/**/angular.js',
//        'fe-assets/lib/**/jquery-ui.min.js',
//        'fe-assets/lib/**/angular-route.js',
//        'fe-assets/lib/**/angular-animate.js',
//        'fe-assets/lib/**/ui-bootstrap-tpls-0.12.1.js',
//        'fe-assets/lib/**/angular-filter.js',
//        'fe-assets/ng/**/app.js',
//        'fe-assets/ng/**/*.js'])
//        .pipe(concat('app.js'))
//        .pipe(gulp.dest('./public/javascripts/'))
//})

//gulp.task('templates:build', function() {
//    gulp.src('fe/_templates/*.html')
//        .pipe(gulp.dest('./public/_templates/'))
//})


gulp.task('fdata:build-local', function() {
    return gulp.src('./front_end_source/fake_data/*.json')
        .pipe(gulp.dest('./public/fake_data/'))
})


gulp.task('js:build-local', function() {
    gulp.src([
        './front_end_source/app/**/*.js'])
        .pipe(gulp.dest('./public/js/app/'));
    gulp.src('./bower_components/angular/angular.js')
        .pipe(gulp.dest('./public/js/lib/angular/'))
    gulp.src('./bower_components/angular-ui-router/release/angular-ui-router.js')
        .pipe(gulp.dest('./public/js/lib/angular/'))
    gulp.src('./bower_components/angular-animate/angular-animate.js')
        .pipe(gulp.dest('./public/js/lib/angular/'))
    gulp.src('./bower_components/bootstrap/dist/js/bootstrap.js')
        .pipe(gulp.dest('./public/js/lib/bootstrap/'))
    gulp.src('./bower_components/angular-cookie/angular-cookie.js')
        .pipe(gulp.dest('./public/js/lib/angular/'))
})

gulp.task('css:build-local', function() {
    gulp.src('./bower_components/bootstrap/dist/css/bootstrap.min.css')
        .pipe(gulp.dest('./public/css/lib/'))
    gulp.src('./front_end_source/less/**/*.less')
        .pipe(less())
        .pipe(gulp.dest('./public/css/'))
})

gulp.task('templates:build-local', function() {
    return gulp.src('./front_end_source/_templates/*.html')
        .pipe(gulp.dest('./public/_templates/'))
})

gulp.task('fonts:build-local', function() {
    gulp.src('./bower_components/bootstrap/dist/fonts/*')
        .pipe(gulp.dest('./public/css/fonts/'))
})

gulp.task('images:build-local', function() {
    gulp.src('./front_end_source/images/*')
        .pipe(gulp.dest('./public/images/'))

})



gulp.task('watch', function() {
    gulp.watch('fe-assets/lib/**/*.js', ['js:build']);
    gulp.watch('fe-assets/css/**/*.css', ['css:build']);
    gulp.watch('fe-assets/templates/*.html', ['templates:build']);
})

gulp.task('default', ['build', 'watch']);

ticketsFactories
    .factory('HttpFactory', [
        '$http'
        ,'$rootScope'
        ,'$q'
        ,'$location'
        ,function($http, $rootScope, $q, $location) {
            return function (config) {
                console.log($location);
                var deffered = $q.defer();

                config.url = '/api/' + config.url;

                $http(config)
                    .success(function (data, status, headers, config) {
                        console.log("success", data);
                        if (data.ErrorMessage) {
                            deffered.reject(data, status, headers, config);
                        } else {
                            deffered.resolve(data, status, headers, config);
                        }
                    })
                    .error(function (data, status, headers, config) {
                        console.log("error", data);
                        deffered.reject(data, status, headers, config);
                    });
                return deffered.promise;
            }
        }
    ])
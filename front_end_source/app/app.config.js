//config file. runs first as angular loads up
tickets
    .config([
        '$locationProvider'
        ,'$stateProvider'
        ,'$urlRouterProvider'
        ,function($locationProvider, $stateProvider, $urlRouterProvider) {

            //set hash bang for SEO purpose
            $locationProvider.hashPrefix('!').html5Mode(true);


            // For any unmatched url, redirect to /state1
            $urlRouterProvider.otherwise('/page-does-not-exist/');
            //


            //set up route
            $stateProvider
                .state('home', {
                    url : '/',
                    templateUrl : '/_templates/home.html',
                    controller : 'HomeController'
                })
                .state('users', {
                    url : '/users/',
                    templateUrl : '/_templates/users.html',
                    controller : 'UsersController'
                })
                .state('about', {
                    url : '/about/',
                    templateUrl : '/_templates/about.html',
                    controller : 'AboutController'
                })
                .state('404', {
                    url : '/page-does-not-exist/',
                    templateUrl : '/_templates/404.html',
                    controller : '404Controller'
                })
        }
    ])
var ticketsFactories = angular.module('ticketsFactories', []);
var ticketsDirectives = angular.module('ticketsDirectives', []);
var ticketsControllers = angular.module('ticketsControllers', []);


var tickets = angular
    .module('tickets', [
        'ui.router'
        ,'ipCookie'
        ,'ngAnimate'
        ,'ticketsFactories'
        ,'ticketsDirectives'
        ,'ticketsControllers'
    ])
/**
 * Created by Kenneth Kogi (kogi.me) on 7/7/15.
 */

// In our app.js

fragseg = function(req, res, next) {
    var fragment = req.query._escaped_fragment_;

    // If there is no fragment in the query params
    // then we're not serving a crawler
    if(!('_escaped_fragment_' in req.query)) return next();


    // If fragment does not start with '/'
    // prepend it to our fragment
    if (fragment.charAt(0) !== "/")
        fragment = '/' + fragment;


    // If the fragment is empty, serve the
    // index page
    if (fragment === "" || fragment === "/")
        fragment = "/index.html";


    // If fragment does not end with '.html'
    // append it to the fragment
    if (fragment.indexOf('.html') == -1)
        fragment += ".html";

    // Serve the static html snapshot
    try {
        console.log('fragment ' + fragment);
        var file = __dirname + "/snapshots" + fragment;
        //res.sendfile(file);
        res.render('statics', {page : fragment});
    } catch (err) {
        res.send(404);
    }
}


module.exports = fragseg;


var mysql  = require('mysql');
var options = {
    connectionLimit   : 10,
    host              : process.env.DBHOST,
    user              : process.env.DBUSER,
    password          : process.env.DBPASS,
    database          : process.env.DBNAME

};
var pool  = mysql.createPool(options);

module.exports = {

    options: options,

    connect: function(req, res, next)
    {
        pool.getConnection(function(err, connection)
        {
            if(err)
            {
                console.log('ERR: '+err);
                res.send('500: DB connection Error', 500);
                res.end();
                return;
            }
            //req.logmsg.info("DB Connection attached");
            connection.on('error', function(err) {
                console.log('MYSQL ERR:');
                res.end('Error Occurred!', 500);
                return;
            });
            req.db = connection;
            req.dbQuery = query;
            next();
        });

        res.on('finish',function(){
            console.log('finished');
            req.db.release();
        })
    }
};

function query(req, res, sql, cb)
{
    req.db.query(sql,function(err,rows){
        if(err)
        {
            console.log(err);
            res.send('Error Occurred!', 500);
            return;
        }
        cb(err,rows);
    })
}
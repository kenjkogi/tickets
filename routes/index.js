var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  var pageInfo = {
    title: 'Ticket Managing System',
    baseUrl: process.env.BASEURL
  };
  res.render('index', pageInfo);
});

module.exports = router;

var express = require('express');
var router = express.Router();

router.get('/', function(req, res, next) {
    res.send('This is the api');
});

router.get('/users/', function(req, res, next) {
    var sql = 'SELECT * FROM user ORDER BY id DESC';

    req.dbQuery(req, res, sql, function(error, rows) {
        var data = [];
        for(var i = 0; i < rows.length; i++) {
            data.push(rows[i]);
        }
        res.json({ "return" : "SUCCESS", "users": data });
    });

});

module.exports = router;
